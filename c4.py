import numpy as np

class Connect4:
    def __init__(self, rows=6, cols=7):
        self.rows = rows
        self.cols = cols
        self.board = np.zeros((rows, cols), dtype=int)
        self.current_player = 1  # Player 1 starts

    def reset_board(self):
        self.board = np.zeros((self.rows, self.cols), dtype=int)
        self.current_player = 1

    def is_valid_move(self, col):
        return 0 <= col < self.cols and self.board[0][col] == 0

    def make_move(self, col):
        for i in range(self.rows - 1, -1, -1):
            if self.board[i][col] == 0:
                self.board[i][col] = self.current_player
                break
        self.current_player = 3 - self.current_player  # Switch player

    def check_winner(self):
        # Check rows
        for i in range(self.rows):
            for j in range(self.cols - 3):
                if (
                    self.board[i][j] == self.board[i][j + 1] == self.board[i][j + 2] == self.board[i][j + 3] != 0
                ):
                    return self.board[i][j]

        # Check columns
        for i in range(self.rows - 3):
            for j in range(self.cols):
                if (
                    self.board[i][j] == self.board[i + 1][j] == self.board[i + 2][j] == self.board[i + 3][j] != 0
                ):
                    return self.board[i][j]

        # Check diagonals (top-left to bottom-right)
        for i in range(self.rows - 3):
            for j in range(self.cols - 3):
                if (
                    self.board[i][j] == self.board[i + 1][j + 1] == self.board[i + 2][j + 2] == self.board[i + 3][j + 3] != 0
                ):
                    return self.board[i][j]

        # Check diagonals (top-right to bottom-left)
        for i in range(3, self.rows):
            for j in range(self.cols - 3):
                if (
                    self.board[i][j] == self.board[i - 1][j + 1] == self.board[i - 2][j + 2] == self.board[i - 3][j + 3] != 0
                ):
                    return self.board[i][j]

        return 0  # No winner

    def display_board(self):
        for row in self.board:
            print("|", end=" ")
            for cell in row:
                print("X" if cell == 1 else "O" if cell == 2 else " ", end=" | ")
            print()
        print("-" * (4 * self.cols + 1))

def main():
    connect4_game = Connect4()

    while True:
        connect4_game.display_board()

        # Get player move
        try:
            move = int(input(f"Player {connect4_game.current_player}, choose a column (0-{connect4_game.cols - 1}): "))
        except ValueError:
            print("Invalid input. Please enter a number.")
            continue

        if not connect4_game.is_valid_move(move):
            print("Invalid move. Choose a different column.")
            continue

        connect4_game.make_move(move)

        winner = connect4_game.check_winner()
        if winner != 0:
            connect4_game.display_board()
            print(f"Player {winner} wins!")
            break

        # Check for a draw
        if np.all(connect4_game.board != 0):
            connect4_game.display_board()
            print("It's a draw!")
            break

if __name__ == "__main__":
    main()
