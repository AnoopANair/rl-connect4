import numpy as np
import random

class Connect4:
    def __init__(self, rows=6, cols=7):
        self.rows = rows
        self.cols = cols
        self.board = np.zeros((rows, cols), dtype=int)
        self.current_player = 1  # Player 1 starts

    def reset_board(self):
        self.board = np.zeros((self.rows, self.cols), dtype=int)
        self.current_player = 1

    def is_valid_move(self, col):
        return 0 <= col < self.cols and self.board[0][col] == 0

    def make_move(self, col):
        for i in range(self.rows - 1, -1, -1):
            if self.board[i][col] == 0:
                self.board[i][col] = self.current_player
                break
        self.current_player = 3 - self.current_player  # Switch player

    def check_winner(self):
        for i in range(self.rows):
            for j in range(self.cols - 3):
                if (
                    self.board[i][j] == self.board[i][j + 1] == self.board[i][j + 2] == self.board[i][j + 3] != 0
                ):
                    return self.board[i][j]

        for i in range(self.rows - 3):
            for j in range(self.cols):
                if (
                    self.board[i][j] == self.board[i + 1][j] == self.board[i + 2][j] == self.board[i + 3][j] != 0
                ):
                    return self.board[i][j]

        for i in range(self.rows - 3):
            for j in range(self.cols - 3):
                if (
                    self.board[i][j] == self.board[i + 1][j + 1] == self.board[i + 2][j + 2] == self.board[i + 3][j + 3] != 0
                ):
                    return self.board[i][j]

        for i in range(3, self.rows):
            for j in range(self.cols - 3):
                if (
                    self.board[i][j] == self.board[i - 1][j + 1] == self.board[i - 2][j + 2] == self.board[i - 3][j + 3] != 0
                ):
                    return self.board[i][j]

        return 0  # No winner

    def display_board(self):
        for row in self.board:
            print("|", end=" ")
            for cell in row:
                print("X" if cell == 1 else "O" if cell == 2 else " ", end=" | ")
            print()
        print("-" * (4 * self.cols + 1))

class QLearningAgent:
    def __init__(self, rows, cols, epsilon=0.1, alpha=0.5, gamma=0.9):
        self.rows = rows
        self.cols = cols
        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma
        self.q_table = np.zeros((rows, cols, cols), dtype=float)

    def choose_action(self, state):
        if np.random.rand() < self.epsilon:
            return random.randint(0, self.cols - 1)  # Explore
        else:
            return np.argmax(self.q_table[state[0], state[1]])

    def update_q_table(self, state, action, reward, next_state):
        best_next_action = np.argmax(self.q_table[next_state[0], next_state[1]])
        self.q_table[state[0], state[1], action] += self.alpha * (reward + self.gamma * self.q_table[next_state[0], next_state[1], best_next_action] - self.q_table[state[0], state[1], action])

def play_game(agent):
    connect4_game = Connect4()
    state = [tuple(map(tuple, connect4_game.board)), connect4_game.current_player]

    while True:
        action = agent.choose_action(state)
        connect4_game.make_move(action)

        next_state = [tuple(map(tuple, connect4_game.board)), connect4_game.current_player]

        winner = connect4_game.check_winner()
        if winner != 0:
            reward = 1 if winner == 1 else -1
            agent.update_q_table(state, action, reward, next_state)
            return winner

        if np.all(connect4_game.board != 0):
            return 0  # Draw

        state = next_state

def train_q_learning_agent(agent, episodes):
    for episode in range(episodes):
        winner = play_game(agent)
        if winner != 0:
            print(f"Episode {episode + 1}, Winner: Player {winner}")
        else:
            print(f"Episode {episode + 1}, It's a draw!")

if __name__ == "__main__":
    rows, cols = 6, 7
    agent = QLearningAgent(rows, cols)
    train_q_learning_agent(agent, episodes=2000)

    # Test the trained agent
    connect4_game = Connect4()
    state = [tuple(map(tuple, connect4_game.board)), connect4_game.current_player]

    while True:
        connect4_game.display_board()
        action = agent.choose_action(state)
        connect4_game.make_move(action)
        state = [tuple(map(tuple, connect4_game.board)), connect4_game.current_player]

        winner = connect4_game.check_winner()
        if winner != 0:
            connect4_game.display_board()
            print(f"Trained Agent Wins! Player {winner}")
            break

        if np.all(connect4_game.board != 0):
            connect4_game.display_board()
            print("It's a draw!")
            break
